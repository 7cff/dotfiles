# dotfiles

> Personal dotfiles for my devices.

This repository contains the dotfiles I use day-to-day on the devices I own.
My working environment consists of the following:

- **Operating System:** [Arch Linux](https://archlinux.org/)
- **Shell:** [zsh](https://www.zsh.org/)
  - **Theme:** [powerlevel10k](https://github.com/romkatv/powerlevel10k)
- **Desktop Environment:** [KDE Plasma](https://kde.org/plasma-desktop/)
- **Window Manager:** [i3-gaps](https://github.com/Airblader/i3)
- **Compositor:** [picom](https://github.com/yshui/picom)
- **Theme:** [wpgtk](https://github.com/deviantfero/wpgtk)
- **Status Bar:** [polybar](https://github.com/polybar/polybar)
- **App Launcher:** [rofi](https://github.com/davatorium/rofi)
- **Notification Manager:** [Linux Notification Center](https://github.com/phuhl/linux_notification_center)

This repository also contains installation scripts and dotfiles for the following:

- [Discord](https://discord.com/) + [Better Discord](https://github.com/rauenzi/BetterDiscordApp) + 
[pywal-discord](https://github.com/FilipLitwora/pywal-discord)
- [intellijPywal](https://github.com/ttpcodes/intellijPywal)

The dotfiles in this repository are managed by [dotdrop](https://github.com/deadc0de6/dotdrop).
There are two profiles configured in this repository:

- **alice-margatroid:** Laptop configuration with 4K resolution.
- **momiji-inubashiri:** Desktop configuration with 1080p resolution.

## Install

Note that due to how the dotfiles are configured, the installation scripts will only work on Arch Linux.

You'll need to install dotdrop and [trizen](https://github.com/trizen/trizen) in order to install these dotfiles.
Once that's done, clone this repository and use dotdrop to install the dotfiles.

```bash
git clone https://aur.archlinux.org/trizen.git
cd trizen/
makepkg -si
cd ../
trizen -S dotdrop
git clone https://github.com/ttpcodes/dotfiles.git
cd dotfiles/
dotdrop install -a -p <profile>
```
